﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luckdraw
{
    public partial class Rotation : UserControl
    {
        public Rotation()
        {
            InitializeComponent();
        }

        public void Initial()
        {
            // pictureBox1.BorderStyle = BorderStyle.None;
            pictureBox1.Image = null;
            lblName.Text = "";
        }

        public void SetNameInfo(name NameInfo)
        {

            this.pictureBox1.Image = Image.FromFile(NameInfo.headImage);
            lblName.Text = NameInfo.Name;

            SetMiddleControl(lblName);

        }
        private void Rotation_Load(object sender, EventArgs e)
        {
            lblName.Parent = pictureBox1;
            lblName.BackColor = Color.Transparent;
            pictureBox1.BackColor = Color.Transparent;
        }

        private void SetMiddleControl(Control contorl)
        {
            contorl.Left = (this.Width - contorl.Width) / 2;
        }


        public void DrawRoundRect(Graphics g, Pen p, float X, float Y, float width, float height, float radius)
        {
            System.Drawing.Drawing2D.GraphicsPath gp = new System.Drawing.Drawing2D.GraphicsPath();

            gp.AddLine(X + radius, Y, X + width - (radius * 2), Y);

            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);

            gp.AddLine(X + width, Y + radius, X + width, Y + height - (radius * 2));

            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);

            gp.AddLine(X + width - (radius * 2), Y + height, X + radius, Y + height);

            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);

            gp.AddLine(X, Y + height - (radius * 2), X, Y + radius);

            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);

            gp.CloseFigure();

            g.DrawPath(p, gp);
            gp.Dispose();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
