﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Luckdraw
{
    public class prize
    {
        public int SN { get; set; }
        public string PrizeName { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int PerTime { get; set; }
        public int Count { get; set; }
        public int Seq { get; set; }
        public bool HeadImage { get; set; }
        public System.Drawing.Color FontColor { get; set; }
        public int FontSize { get; set; }
    }

    public class name
    {
        public int SN { get; set; }

        public string Name { get; set; }

        public int Probabilities { get; set; }

        public string ForPrizelist { get; set; }

        public string headImage
        {
            get
            {
                string headimageurl = Application.StartupPath + @"\HeadImages\" + Name + ".jpg";
                if (!File.Exists(headimageurl))
                    headimageurl = Application.StartupPath + @"\HeadImages\guest.jpg";
                return headimageurl;
            }
        }
    }
    public class bingo
    {
        public string Name { get; set; }
        public  string PrizeName { get; set; }
    }

}
